# Framaclic

Let's see a quick example. Fred likes to write texts and spreads them all over the web.

He has two problems with that.

His texts are on different sites with sometimes web addresses (or "URLs") as long as a day without bread.
But for that he found the parade, it's frama.link, the Framasoft URL shortener.
He created a short address for each of his texts, and when asked where one can read his prose, he gives this URL rather than an address of 256 biscornus characters.
For even shorter web addresses, he could use https://huit.re/.

Fred's other concern: he writes for fun, he publishes under free license, he understood that he would not live with his pen, but he can't help but wonder if someone really reads what he writes.

Fred is all happy when Framasoft releases Framaclic (well, he doesn't do triple saltos, but he has a moment of jubilation).

## What's that?

![Zag, l'adorable mascotte de Dolomon](img/dolomon.png)

Framaclic is a URL shortener that counts clicks. There you go. When you put it that way, it sounds pretty simple, doesn't it?

Well, good news: it's simple!

Well, let's be fair, Frama.link already had a counter, but a rudimentary one.
It recognizes the author of the short URL via a small cookie and is able to provide a count of clicks.
But it only works from the computer and browser on which the short address was created (because of the cookie).

Framaclic is a steroid-doped Frama.link.

## How it works?

Framaclic is based on Dolomon, as DOwnLOad MONitor.

Fred goes to framaclic.org.
He creates an account with a password, to be the only one to be able to access his statistics (sometimes they are bad).

He makes a list of the addresses of all the resources to which he wants to create a link: his texts, his blog, his CV, his photo galleries, a comic strip of Simon he loves to share with his colleagues...
If the list is not exhaustive, it's okay, he can add some later.

Since he likes things to be arranged correctly (reminder: this example is a fiction), he creates categories and labels to find his way around.
Especially when he tells himself that this thing will do him a great service and that he will end up putting a lot of addresses there.

Then, for each long address it generates a short one (a "dolo").
No need to keep it, Framaclic takes care of it.

![dolo chien](img/addDoloChien-1.png)

Dolos are created progressively.

![ajout dolo](img/addDoloVFVVOK.png)

To follow visits on a specific page, Fred can create a dolo pointing to a small transparent image (Dolomon offers you one) and insert the URL generated, like inserting an image, in his page.

Fred especially likes to create dolos that point to a document, instead of the web page.
For instance, a dolo for the pdf of his novel (http://framabook.org/docs/vieuxflic/FredUrbain_VFVV_cczero20151108.pdf instead of the generic page https://framabook.org/vieux-flic-et-vieux-voyou/), another one for the e-pub version, and another one for the Latex source code.
This way Fred will know which version is the most downloaded.

![liste dolos](img/listDolos.png)

But he won't know anything more: Framaclic only records anonymous statistics, not the IP addresses of the visitors.

On the other hand, it does make beautiful graphics:

![graph framaclic](img/graph-framaclic-1024x307.png)

And as your data belongs to you, you can download them in a CSV file, which will allow you to manipulate them as you wish, to make coloured pie charts...

Ah, one last cool thing to know: Luc made a Dolomon plugin for WordPress.
If you have a blog, you can create your dolos directly from your article.

![](https://framaclic.org/h/doc-framaclic)
