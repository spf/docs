Démarrer sur diaspora\*
=======================

2ème partie - L'interface
-------------------------

**Note:** si vous utilisez un appareil portatif, vous verrez l'interface diaspora\* conçue pour les petits écrans. [Cliquez ici](2b-interface-mobile.html) pour un guide de l'interface mobile.

Vous voilà maintenant devant une interface que vous ne connaissez pas
encore (si vous êtes toujours sur la page de modification de profil,
cliquez sur le logo "astérisque" ou sur **Flux**, tout à gauche de la barre
d'en-tête noire). Examinons la disposition et familiarisons-nous avec.

De par la nature décentralisée et libre de diaspora\*, les
administrateurs de pods peuvent personnaliser leurs pods. Ainsi, selon
le pod sur lequel vous vous connectez, l'interface peut être légèrement
différente de ce que vous trouverez décrit ici. Toutefois, les éléments
essentiels devraient être tous présents, même s'ils sont visuellement
organisés légèrement différemment.

En haut de l'écran se trouve la barre d'en-tête restant visible lorsque
vous faites défiler la page. Celle-ci contient :

![Dropdown](images/interface-1.png)

1.  le nom de votre pod qui vous renvoie à la page d'accueil
2.  liens vers votre **Flux** et **Mon activité**
3.  l'icône de notifications (expliquée dans la [partie
    6](6-partage.html));
4.  l'icône de discussion (aussi expliquée dans la [partie
    6](6-partage.html));
5.  la barre de recherches (pour en savoir plus, voire la [partie
    4](4-echange.html));
6.  le menu personnel avec :
    -   un lien vers votre page de profil (voir plus bas)
    -   un lien vers votre liste de contacts (voir [partie
        4](4-echange.html));
    -   un lien vers les paramètres de votre compte (voir [partie
        7](7-fin.html));
    -   un lien vers la section **Aide**
    -   un bouton pour se déconnecter.

Sous l'en-tête, on distingue deux colonnes :

![Stream](images/interface-2.jpg)

 

-   Sur la gauche, vous verrez des liens vers les contenus spéciaux
    disponibles dans diaspora\* : **Flux**, **Mon activité**, **@Mentions**, **Mes
    aspects** et **\#Tags suivis**, **Activité publique**. Nous allons vous expliquer tout cela.
-   La colonne centrale présente le flux de contenu avec une "boîte de
    publication" au-dessus. Nous détaillerons cela dans la [Partie
    5](5-partage.html).

Les différentes vues disponibles dans diaspora\* sont :

#### Le flux

Le **Flux** est votre page d'accueil sur diaspora\*. Elle agrège tous les posts disponibles pour vous. A savoir :

1.  les posts de vos contacts
2.  les posts publics contenant un \#tags que vous suivez
3.  les posts qui vous mentionnent
4.  posts provenant de « l'actualité de la communauté » si votre pod a activé cette fonctionnalité (voir [Partie
    7](7-fin.html)).

Les messages de votre flux sont triés selon la date du message
d'origine.

#### Mon activité

Votre flux **Mon activité** montre tous les messages que vous avez aimés
ou commentés. C'est une façon pratique de garder un lien avec toutes les
interactions que vous avez pu avoir sur diaspora\*. Les messages y sont
triés en fonction de la dernière activité (commentaire ou "j'aime") sur
chacun d'eux.

#### Les @mentions

Vous trouverez ici la liste de tous les messages écrits par d'autres
personnes et qui vous @mentionnent. Nous expliciterons les mentions ici
[Partie 4](4-echange.html).

#### Mes aspects

**Mes aspects** affiche tous les messages envoyés par les personnes avec
lesquelles vous partagez - c'est à dire les personnes que vous avez
placées dans vos aspects. Ne vous inquiétez pas si vous ne savez pas
encore ce que sont les aspects, nous l'expliquons en détail dans la
partie suivante.

Cliquez sur **Mes aspects** pour voir la liste de vos aspects. Vous pouvez
voir les articles publiés dans tous vos aspects, ou seulement dans un ou
plusieurs d'entre eux en les (dé)sélectionnant dans la colonne de
gauche.

#### \#Tags suivis

Cette vue affiche tous les messages disponibles, qu'ils soient publics
ou qu'ils aient été publiés dans des aspects dans lesquels vous avez été
placé(e), contenant les \#tags que vous suivez. Nous aborderons les
\#tags en détail dans la [Partie
5](5-partage.html).

Cliquez sur \#tags suivis pour voir la liste de vos tags. Tout comme Mes
aspects, vous pouvez filtrer le flux pour n'afficher que les articles
qui contiennent un ou plusieurs tags en les (dé)sélectionnant dans la
colonne de gauche.

#### Activité publique

Le flux **Activité publique** affiche tous les posts publics accessibles à votre *pod*, mêmes ceux créés par des personnes avec qui vous ne partagez pas ou ne contenant aucuns des tags que vous suivez. Cela fait beaucoup de contenu !

### Autres vues

Il y a d'autres vues disponibles sur diaspora\*. Vous pouvez les trouver en cliquant ces liens.

#### Page de tags

Cliquez sur n'importe quel tag pour vous retrouver sur la page de celui-ci. Cela vous montrera tous les posts contenant ce tag, que votre *pod* connaît.

#### Single-post view

Cliquez sur la date d'un post pour le voir en « single-post view ». Si vous voulez envoyer le lien d'un post, c'est ce lien qu'il faut donner.

#### Vue du profil

En cliquant sur votre photo ou votre nom partout où vous le voyez vous
reviendrez à votre page de profil . Y sont présentés tous les messages
que vous avez publié sur diaspora\*. Vous verrez également votre photo
de profil, votre nom d'utilisateur, les tags "à propos de moi" et votre
ID diaspora\* (dont il est question à la rubrique "Inscription" dans la
[partie 1](1-connexion.html#inscription)).

Sous votre photo de profil vous verrez toutes les informations
personnelles que vous avez indiquées telles que votre date
d'anniversaire, l'endroit où vous habitez, votre genre, votre
biographie, etc.

Cliquez sur le bouton bleu Modifier mon profil si vous souhaitez changer
une information de votre profil.

Vous pouvez également consulter la page de profil de quelqu'un en
cliquant sur son nom ou sa photo. La quantité d'informations auxquelles
vous aurez accès dépend de votre relation avec cette personne. En haut à
droit, vous verrez un bouton "aspects" : gris si la personne n'est pas
dans vos aspects, vert si elle s'y trouve. Cliquez dessus pour ajouter
la personne à un aspect.

-   Sous leur photo de profil, vous pouvez voir une barre d'icônes :
    grise (avec un cercle) s'ils ne partagent pas avec vous, verte (avec
    une ✓) s'ils partagent, rouge si vous ignorez cette personne. La
    barre contiendra les icônes suivantes, si elles vous sont
    disponibles : une icône @mention si vous partagez avec cette
    personne et une icône de message si vous partagez mutuellement.
-   De plus, dans la colonne de gauche se trouve un lien vers les photos
    envoyées par cette personne ainsi que tous ses contacts qu'elle vous
    aura rendu visibles (ceci est expliqué dans la section suivante
    "Aspects").

Sous le nom et l'``id`` de la personne se trouvent des onglets pour afficher leurs posts, leurs photos envoyées sur le *pod* et les contacts visibles par vous (expliqué dans la partie suivante, « Aspects »).

#### Thèmes

Vous pouvez personnaliser l'interface de diaspora\* en utilisant un thème (voir [partie
7](7-fin.html)). les thèmes suivants sont disponibles :

-   **Original dark**: le thème par défaut de diaspora\*
-   **Original white background**: le thème par défaut, mais avec le fond blanc. Cela ressemble à « l'ancien diaspora\* »
    interface
-   **Dark green**
-   **Magenta**
-   **Egyptian blue**

Votre *pod* a peut-être créé un thème personnalisé. Si c'est le cas vous le trouverez sous la liste des thèmes présentés ci-dessus.



C'est tout ce que vous avez besoin de savoir concernant l'interface pour
le moment. La première chose que vous voudrez faire c'est de trouver des
personnes avec lesquelles partager. Toutefois, avant cela,
concentrons-nous sur une fonctionnalité importante sur diaspora\* : les
aspects.

 

[Partie 1 –
Connexion](1-connexion.html) |
[3ème partie - Les
aspects](3-aspects.html)

#### Ressources utiles

-   [Base de code](http://github.com/diaspora/diaspora/)
-   [Documentation](https://wiki.diasporafoundation.org/)
-   [Trouver et signaler des
    bugs](http://github.com/diaspora/diaspora/issues)
-   [IRC - Général](http://webchat.freenode.net/?channels=diaspora)
-   [IRC -
    Développement](http://webchat.freenode.net/?channels=diaspora-dev)
-   [Liste - Général](http://groups.google.com/group/diaspora-discuss)
-   [Liste - Développement](http://groups.google.com/group/diaspora-dev)

[![Licence Creative
Commons](images/cc_by.png)](http://creativecommons.org/licenses/by/3.0/)
[diasporafoundation.org](https://diasporafoundation.org/) est protégé
sous licence [Licence publique Creative Commons Attribution 3.0 non
transposée](http://creativecommons.org/licenses/by/3.0/)
