# Identification
_____

Vous pouvez vous identifier dans votre équipe à partir de l'adresse web `https://framateam.org/teamname`.

## Méthodes pour s'identifier

Il existe plusieurs options pour s'identifier dans votre équipe selon la manière dont votre administrateur système a configuré votre serveur.

#### Identification par adresse de courriel ou nom d'utilisateur

Quand vous êtes autorisé par votre administrateur système, vous pouvez vous identifier avec le nom d'utilisateur ou l'adresse de courriel utilisée pour la création du compte.

Si vous avez oublié votre mot de passe, vous pouvez le réinitialiser en cliquant sur l'onglet **J'ai oublié mon mot de passe** sur la page d'identification, ou en contactant votre administrateur système pour obtenir de l'aide afin de réinitialiser votre mot de passe.

#### GitLab Single-Sign-On (SSO)

Une fois autorisé par votre administrateur système, vous pouvez vous identifier avec votre compte GitLab en utilisant l'identification en un clic. GitLab SSO vous permet de créer des équipes, créer des comptes dans des équipes, et de vous identifier dans des équipes avec un nom d'utilisateur, une adresse de courriel et un mot de passe uniques qui marchent pour tout le serveur.

## Changer d'équipe

Vous pouvez passer d'une équipe à l'autre en utilisant la barre latérale d'équipe qui apparaît à gauche de votre liste de chaînes sur la barre latérale de gauche.

![](../../images/getting-started_signing-in_change-team.png)

## Se déconnecter

Vous pouvez vous déconnecter depuis le **Menu Principal**, accessible en cliquant sur les trois points dans le bandeau supérieur, sur le côté gauche de l'écran. Cliquer sur «&nbsp;Déconnexion&nbsp;» vous déconnecte de toutes les équipes où vous étiez identifié et ouvertes dans votre navigateur.

## Configuration iOS

Vos équipes Mattermost sont accessibles sur les terminaux iOS en téléchargeant l'application Mattermost App.

  1. Ouvrez l'App Store sur votre appareil Apple avec iOS 9.0 ou plus.
  2. Cherchez « Mattermost » et cliquez sur **OBTENIR** pour télécharger gratuitement l'application.
  3. Ouvrez Mattermost depuis votre écran d'accueil et renseignez-y votre équipe ainsi que vos identifiants de compte pour vous connecter :
      1. Entrez l'URL de l'équipe : c'est une partie de l'adresse web de votre équipe sur le domaine. Vous pouvez obtenir l'URL de l'équipe en demandant à votre administrateur système ou en regardant dans la barre d'adresse dans un navigateur internet avec Mattermost ouvert. Elle se présente sous la forme `https://framateam.org/teamurl/`.
      2. S'identifier dans Mattermost : il s'agit de vos identifiants de compte comme décrit dans l'une des méthodes ci-dessus.

## Configuration pour Android

Vos équipes Mattermost sont accessibles sur les terminaux android en téléchargeant l'application Mattermost App.

  1. ouvrez le *play store* sur votre appareil Android
  * cherchez « [Mattermost](https://play.google.com/store/apps/details?id=com.mattermost.rn) » et cliquez sur **Installer** pour télécharger gratuitement l'application
  * ouvrez Mattermost depuis votre écran d'accueil et renseignez-y votre équipe ainsi que vos identifiants de compte pour vous connecter :
    1. entrez l'URL de l'équipe : c'est une partie de l'adresse web de votre équipe sur le domaine. Vous pouvez obtenir l'URL de l'équipe en demandant à votre administrateur système ou en regardant dans la barre d'adresse dans un navigateur internet avec Mattermost ouvert. Elle se présente sous la forme `https://framateam.org/`
    * s'identifier dans Mattermost : il s'agit de vos identifiants de compte comme décrit dans l'une des méthodes ci-dessus
