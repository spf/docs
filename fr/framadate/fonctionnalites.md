# Fonctionnalités

## Modifier la date d'expiration

Pour prolonger la date d'expiration vous devez&nbsp;:

  * utiliser le lien d'administration (celui finissant par `/admin`)
  * passer la souris sous **Date d'expiration** et cliquer sur l'icône "crayon" <i class="fa fa-pencil" aria-hidden="true"></i>
  * modifier la date
  * cliquer sur l'icône de validation <i class="fa fa-check-square" aria-hidden="true"></i>

![gif prolongation date](img/fonctionnalites_prolongation_date.gif)

Vous pouvez cliquer sur celle d'annulation <i class="fa fa-times" aria-hidden="true"></i> si vous ne souhaitez finalement pas modifier la date.

## Ajouter une colonne

Il n'est pas possible de **modifier** une colonne : vous devez la supprimer en cliquant sur <i class="fa fa-times" aria-hidden="true"></i> au-dessus de celle-ci et en créer une nouvelle&nbsp;:

  * en cliquant sur l'icône **+**

  ![ajouter une colonne date](img/fonctionnalites_date_ajout_colonne.png)

  * en donnant un nom à cette colonne

  ![donner un nom à la colonne](img/fonctionnalites_date_ajout_nom_colonne.png)

![nouvelle colonne date](img/fonctionnalites_date_colonne_ajoutee.png)

## Ajouter un intervalle de date et reporter les horaires du premier jour sur les autres jours

Pour cela vous devez choisir l'intervalle de dates avec l'icône **++**&nbsp;:

![image choix intervalle de date](img/date_date_multiples.png)

puis positionner le curseur dans le premier horaire du premier jour et cliquer sur&nbsp;:

![image report de date](img/date_report.png)

![gif montrant l'ajout de dates et le report d'horaires](img/date_multi.gif)

## Supprimer un Framadate

Pour supprimer un Framadate vous devez utiliser le lien d'administration (celui qui finit par `/admin` - [voir comment retrouver ce lien](https://contact.framasoft.org/fr/faq#date_sondage_perdu)) puis&nbsp;:

  1. cliquer sur l'icône <i class="fa fa-trash" aria-hidden="true"></i>
  * cliquer sur **supprimer le sondage**
  * cliquer sur le bouton de confirmation **Supprimer le sondage**

![image montrant comment supprimer le Framadate](img/date_suppression.png)
