# Prise en main

## Exporter et importer un pad

Vous avez la possibilité de transférer un pad avec son historique et ses couleurs d'un pad à un autre (voire d'un compte MyPads à un autre) en utilisant le format `etherpad`. Pour ce faire&nbsp;:

  * cliquez sur l'icône <i class="fa fa-exchange" aria-hidden="true"></i>
  * sélectionnez&nbsp;: <i class="fa fa-file-powerpoint-o" aria-hidden="true"></i> Etherpad
  * enregistrez le fichier sur votre ordinateur
  * créez un nouveau pad (dans votre autre compte MyPads par exemple)
  * cliquez à nouveau sur  <i class="fa fa-exchange" aria-hidden="true"></i>
  * cliquez sur le bouton **Parcourir…**
  * sélectionnez le fichier précédemment enregistré
  * cliquez sur le bouton **IMPORTER MAINTENANT**
  * à la question **Importer un fichier écrasera le contenu actuel du pad. Êtes-vous sûr de vouloir le faire ?** cliquez **OK**
  * votre pad est alors remplacé par l'import
