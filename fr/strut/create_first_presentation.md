# Créer sa première présentation

Une fois enregistré sur Framaslides, cliquez sur « Créer une nouvelle présentation ». Vous êtes redirigé vers l'interface d'édition de diapositives.

## Manipuler les diapositives

Il est possible de créer une nouvelle diapositive en cliquant sur le bouton « + » dans le panneau de droite.

L'ordre d'affichage des diapositives est défini de haut en bas mais il est possible de les réorganiser par glisser-déposer. La diapositive active est encadrée en vert.

Il est possible de faire un copié-collé d'une diapositive et d'en supprimer une en utilisant les actions du menu ou bien [les raccourcis clavier](shortcuts.md).

<div class="text-center">
<img src="images/slides_list.png" alt="Liste des diapositives"/>
</div>

## Ajouter un élément

Une première diapositive est ajoutée au projet. Vous pouvez ajouter différents éléments sur une diapositive comme :

* Du texte formaté
* Une image
* Une vidéo
* Un site web
* Des formes basiques

Commençons par ajouter un peu de texte : cliquez sur le bouton « Texte » en haut de l'interface.

<div class="text-center">
<img src="images/elements_text.png" alt="Bouton texte"/>
</div>

Un élément texte apparaît sur votre diapositive et vous pouvez alors notamment le déplacer par glisser-déposer.

<div class="text-center">
<img src="images/elem_text.png" alt="Élément texte"/>
</div>

Pour éditer le texte, double-cliquez dessus : le texte par défaut est sélectionné et la barre de formatage apparaît. Vous pouvez donc écrire votre texte en lieu et place du texte par défaut.

<div class="text-center">
<img src="images/elem_text_edit.png" alt="Élément texte en mode édition"/>
</div>

## Formater le texte

Le texte peut être mis en **gras** ou en *italique*. La taille du texte peut être définie à travers une liste déroulante, mais vous pouvez aussi tout simplement modifier la taille de l’élément en sortant du mode édition en cliquant ailleurs, puis en faisant glisser la double flèche diagonale en bas à droite de l’élément.

La police du texte peut être choisie parmi une sélection de polices prédéfinies. Notez que certaines polices ne s'appliqueront pas si elles ne sont pas installées sur le système. Il en va de même si vous présentez votre présentation sur un appareil différent sur lequel la police est manquante, le navigateur choisira alors une police par défaut.

La couleur du texte peut être choisie avec un outil dédié.

<div class="text-center">
<img src="images/color_selector.png" alt="Sélecteur de couleur"/>
</div>

Des options de mise en page plus complexes sont disponibles, comme créer des listes à puces ou bien numérotées. Il est également possible de définir l'alignement du texte entre « aligné à gauche » et « centré ».

Enfin, vous pouvez créer des liens en cliquant sur le bouton avec l'icône correspondante et en renseignant l'adresse du lien dans la fenêtre modale qui s'ouvre.

Toutes ces options de formatage de textes peuvent être remises à zéro en cliquant sur le dernier bouton de la barre d'outils, qui efface le formatage.

## Manipulations d'élément

### Actions de base

Toute opération de déplacement peut être annulée ou refaite à partir du menu ou bien par les [raccourcis clavier](shortcuts.md). De même que pour les diapositives, les éléments peuvent être coupés ou copiés collés depuis une même diapositive ou bien une diapositive différente. Enfin, il est possible de supprimer un élément en cliquant sur sa croix rouge en haut à gauche.

### Déplacements

Pour déplacer un élément sur la diapositive, glissez-déposez l’élément. Pour un déplacement plus contrôlé, vous pouvez maintenir la touche Maj pour faire se déplacer l’élément par paliers.

Afin de recentrer l’élément verticalement et horizontalement, vous pouvez cliquer sur les flèches avec point d'arrêt dans les deux sens.

### Rotations et transformations

Tout élément peut subir une rotation à l'aide de la flèche circulaire du côté gauche, ainsi que que des transformations 3D à l'aide des doubles flèches dans le sens vertical et horizontal. Notez toutefois que les vidéos et sites web seront rendus sans ces transformations.

## Fond de la diapositive ou surface

Il y a deux possibilités pour changer le fond :

* Modifier le fond de la diapositive ;
* Modifier la « surface » de la diapositive.

La différence entre les deux est que la surface concerne tout l'écran, alors que le fond de la diapositive correspond uniquement à la zone éditable.

Les deux ont un choix de couleurs prédéfinies ainsi qu'une palette pour choisir une couleur personnalisée. Il est également possible d'utiliser une image comme fond.

<div class="text-center">
<img src="images/background_selector.png" alt="Sélecteur de fond"/>
</div>

## Éditeur de transitions

Framaslides est un éditeur de présentations un peu particulier car il ne permet pas de choisir le type de transition entre chaque diapositive, mais permet plutôt d'organiser ses diapositives les unes par rapport aux autres.

<div class="text-center">
<img src="images/btn-panorama.png" alt="Bouton panorama"/>
</div>

Par défaut, toutes les diapositives se suivent horizontalement de gauche à droite. Pour modifier cela, glissez-déposez les diapositives à des endroits différents. Vous pourriez par exemple décider de faire se suivre les diapositives verticalement de haut en bas.

Notez que l'alignement contrôlé à l'aide de la touche majuscule est ici également disponible. Comme pour les éléments, il est possible d'effectuer des rotations et des transformations.

## Sauvegarder sa présentation

Pour sauvegarder la présentation, cliquez pour ouvrir le menu principal puis cliquez sur « Enregistrer sous... ».

<div class="text-center">
<img src="images/menu_save_as.png" alt="Menu pour sauvegarder la présentation"/>
</div>

Entrez ensuite le nom de la présentation à enregistrer.

<div class="text-center">
<img src="images/dialog_save_as.png" alt="Modale pour sauvegarder la présentation"/>
</div>

Puis validez.

## Prévisualiser sa présentation

Pour voir l'aperçu d'une présentation, il est nécessaire que celle-ci ait déjà été sauvegardée. Vous pouvez alors cliquer sur le bouton vert « Aperçu » en haut à gauche de l'interface.

<div class="text-center">
<img src="images/btn-apercu.png" alt="Bouton aperçu"/>
</div>

Vous pouvez ensuite naviguer entre les diapositives en appuyant sur la barre d'espace ou bien les flèches directionnelles.
