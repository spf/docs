# Prise en mains

## Ajouter Framabee à la liste des moteurs disponibles dans la barre de recherche

Pour ajouter Framabee comme moteur de recherche directement dans votre Firefox, vous devez cliquer sur la roue crantée (**1**) puis sur **install in browser** (**2**)&nbsp;:

![Image installation dans Firefox](images/bee_install_browser.png)

Vous devez ensuite accepter d'installer le moteur de recherche&nbsp;:

![Image pop up firefox installation framabee](images/bee_install_browser_popup.png)

Enfin, vous devez vous rendre dans les préférences de Firefox pour lui indiquer d'utiliser Framabee comme moteur par défaut&nbsp;:

![Framabee par défaut dans Firefox](images/bee_pref_firefox.png)
