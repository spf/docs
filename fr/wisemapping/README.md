# Framindmap

[Framindmap](https://framindmap.org/) permet de créer et partager des cartes mentales (aussi appelées « cartes heuristiques »).

![carte de présentation](images/framindmap.png)

---

## Tutoriel vidéo

<div class="text-center">
  <p><video width="420" height="340" controls="controls" preload="none">
      <source src="https://old.framatube.org/files/1288-tutoriel-video-wisemapping-claire-cassaigne.mp4" type="video/mp4" />
      <source src="https://old.framatube.org/files/1288-tutoriel-video-wisemapping-claire-cassaigne.webm" type="video/webm" />
  </video></p>
  <p>→ La <a href="https://old.framatube.org/files/1288-tutoriel-video-wisemapping-claire-cassaigne.webm">vidéo au format webm</a></p>
</div>

Vidéo réalisée par Claire Cassaigne pour le site [FenetreSur](http://fenetresur.wordpress.com/).

## Pour aller plus loin :

* Testez [Framindmap](https://framindmap.org/)
* [Fonctionnalités](fonctionnalites.md)
* Installez [Wisemapping sur votre propre serveur](http://framacloud.org/cultiver-son-jardin/installation-de-wisemapping/)
* Contribuez [au code de Wisemapping](http://www.wisemapping.com/)
