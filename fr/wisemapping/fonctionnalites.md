# Fonctionnalités

## Raccourcis

Vous pouvez voir les raccourcis pour Framindmap avec compte sur la page : https://framindmap.org/c/keyboard

## Publier une carte

Pour publier une carte mentale vous devez :

* cliquer sur l'icône **Publier** : ![icône Publier](images/mindmap_publier.png)
* cocher **Activer le partage**

Si vous souhaitez *embarquer* la carte sur un site, vous devez alors copier le code du style :

```
<iframe style="width:600px;height:400px;border: 1px
solid black" src="https://framindmap.org/c/maps/CHIFFRES/embed?zoom=1"> </iframe>
```
Sinon, vous pouvez cliquer sur l'onglet **URL Publique** et copier l'url de votre carte pour la donner aux personnes souhaitées.

N'oubliez pas de cliquer sur **Accepter** pour confirmer la publication de votre carte.

## Enregistrer une carte sur son ordinateur

### Avec Wisemapping (nécessitant un compte)

Vous devez :
  * cliquer sur l'icône **Exporter**  
  ![icone Exporter mindmap](images/wisemapping_sauvegarde.png)
  * sélectionner le format souhaité
  * cliquer sur **Accepter**

### Avec Mindmaps (sans compte)

Vous devez :

  * cliquer sur **Carte mentale**
  * cliquer sur **Enregistrer**

  ![mindmap sauvegarde](images/mindmap_sauvegarde.png)
    * si vous souhaitez enregistrer sur votre **navigateur**, vous devez cliquer sur **Enregistrer** dans la partie **Stockage local** (attention, vous n'aurez plus la carte si vous changez de navigatuer ou si vous videz le cache)
    * si vous souhaitez l’enregistrer sur votre **ordinateur** vous devez cliquer sur **Enregistrer** dans la partie **Dans un fichier**

## Partager une carte

Pour partager une carte, il faut que toutes les personnes aient un compte Framindmap. Sur la carte que vous souhaitez partager&nbsp;:

  * cliquez sur l'icône ![icone partage framindmap](images/wisemapping_partage_icon.png)
  * entrez l'adresse mail du compte (**1**)
  * optionnel : passez de la possibilité pour la personne d'éditer la carte (**Peut éditer**) à **Peut regarder**
  * cliquez sur **Ajouter** (**2**) pour envoyer un mail de partage à l'adresse ajoutée
  * cliquez sur **Accepter** (**3**)
    ![image procédure partage framindmap](images/wisemapping_partager.png)

## Renommer une carte

Pour renommer une carte vous devez cocher la case devant son nom dans [la liste de vos cartes](https://framindmap.org/c/maps/) puis&nbsp;:

![gif animé montrant la procédure pour renommer une carte](images/mindmap_rename.gif)

  1. cliquer sur **Plus**
  * cliquer sur **Renommer**
  * changer le nom de la carte
  * cliquer sur le bouton **Renommer**

## Retour à la ligne dans un nœud

### Avec Wisemapping (nécessitant un compte)

Vous devez faire `ctrl`+`Entrée` pour aller à la ligne (`Entrée` simplement enregistre le nœud).

### Avec Mindmaps (sans compte)

Vous devez faire `maj`+`Entrée` pour aller à la ligne (`Entrée` simplement enregistre le nœud).
