# Synchronisation avec Windows Phone
[<i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil](../README.md)

Dans votre navigateur, accédez à l'application Calendrier Framagenda. Sous **Paramètres & Importation**, copiez l'**Adresse CalDAV iOS/macOS** dans votre presse-papiers.

  1. Lancez l'application Calendrier Windows 10. Ensuite, cliquez sur l'icône des paramètres (icône "roue crantée") et sélectionnez **Gérer les comptes**.
  - Cliquez sur **Ajouter un compte** et choisissez **iCloud**.
  - Entrez un courriel, un nom d'utilisateur et un mot de passe. Aucune de ces informations n'a besoin d'être valide - elles seront toutes modifiées au cours des prochaines étapes.
  - Cliquez sur **Terminé**. Un message devrait apparaître indiquant que les réglages ont été sauvegardés avec succès.
  - Dans le menu **Gérer les comptes**, cliquez sur le compte iCloud créé lors des étapes précédentes et sélectionnez **Modifier les paramètres**. Cliquez ensuite sur **Modifier les paramètres de synchronisation de la boîte aux lettres**.
  - Faites défiler jusqu'en bas de la boîte de dialogue, sélectionnez **Paramètres avancés de la boîte aux lettres**. Collez l'URL de votre CalDAV dans le champ "Serveur du calendirer (CalDAV)".
  - Cliquez sur **Terminé**. Entrez votre nom d'utilisateur et votre mot de passe Framagenda dans les champs appropriés, et changez le nom de votre compte comme vous le souhaitez (par exemple "Calendrier Framagenda"). Cliquez sur **Enregistrer**.

Après avoir suivi toutes ces étapes, votre calendrier Framagenda devrait se synchroniser. Sinon, vérifiez votre nom d'utilisateur et votre mot de passe. Sinon, répétez ces étapes.

<p class="alert-info alert">
<b>REMARQUE</b> : Vous ne pourrez pas synchroniser votre calendrier si l'authentification à deux facteurs est activée. Suivez les étapes ci-dessous pour obtenir un mot de passe d'application qui peut être utilisé avec l'application client Calendar :
</p>

  1. Connectez-vous à Framagenda. Cliquez sur votre icône d'utilisateur, puis cliquez sur **Paramètres**.
  - Cliquez sur **Sécurité**, puis localisez un bouton intitulé **Générer mot de passe de l'application**. A côté de ce bouton, entrez "Windows 10 Calendar app". Ensuite, cliquez sur le bouton et copiez et collez le mot de passe. Utilisez ce mot de passe au lieu de votre mot de passe Framagenda pour l'étape 8.
