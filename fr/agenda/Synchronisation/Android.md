# Synchronisation avec Android
[<i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil](../README.md)

Nous vous conseillons l'application **DAVx⁵** pour la synchronisation sur Android. D'autres clients comme **CalDAV Sync Adapter** et **aCalDAV** sont succeptibles de fonctionner, mais DAVx⁵ a certains avantages par rapport aux autres applications.

## Installation

DAVx⁵ est disponible gratuitement sur [F-Droid](https://f-droid.org/packages/at.bitfire.davdroid/) ([Tutoriel](../Divers/FDroid.md) d'installation de F-Droid) et au prix de 3.99€ (merci de soutenir son développeur) sur [Google Play](https://play.google.com/store/apps/details?id=at.bitfire.davdroid).

<p class="alert-info alert">
<b>Note</b> : Si vous comptez synchroniser vos listes de tâches (<a href="#synchronisation-de-la-liste-de-tâches" title="Lien vers la synchro des tâches">voir plus bas<a/>), il est recommandé d'installer l'application OpenTasks avant d'installer DAVx⁵.
</p>

## Synchronisation des contacts et des agendas

* Ouvrez l'application DAVx⁵
* Sélectionnez **Ajouter un compte** (le bouton + en bas à droite).
* Sélectionnez **Connexion avec une URL et un nom d'utilisateur**

![Ajouter un compte - DAVx⁵](../images/davdroid-1.png)

<p class="alert-info alert">
<b>Note</b> : Si vous utilisez <a href="../Inscription-Connexion.md#facultatif-utiliser-lauthentification-en-deux-tapes-2fa">l'authentification en deux étapes</a>, vous devez créer <<a href="../Inscription-Connexion.md#utiliser-les-mots-de-passe-dapplication">un mot de passe d'application</a>.
</p>

* Entrez comme URL de base `https://framagenda.org/remote.php/dav/`, puis votre nom d'utilisateur ainsi que votre mot de passe sur Framagenda.
* Enfin, sélectionnez **Se connecter**.
* Il est conseillé de renseigner son adresse email pour vous identifier comme organisateur ou participant à des événements. Ne changez pas l'autre préférence.
* Une fois l'assistant terminé, cliquez sur le compte créé nouvellement dans DAVx⁵.

<p class="alert-warning alert">DAVx⁵ ne devrait pas vous demander de vérifier la signature de Framagenda. Si c'est le cas, cela veut soit dire que vous ne vous connectez pas directement à Framagenda (par exemple si connexion via un portail captif du type wifi gratuit ou bien réseau d’entreprise), ou bien que votre appareil ne reconnaît pas le certificat de framagenda.org, ce que vous pourrez confirmer en vous connectant à framagenda.org via votre navigateur web.</p>

![Contacts & Calendrier - DAVx⁵](../images/davdroid-2.png)

Pour activer la synchronisation des contacts, appuyez simplement sur la case à cocher Contacts dans la section CardDAV.

Pour activer la synchronisation des agendas, cochez les cases correspondant à vos agendas dans la section CalDAV. Notez que l'agenda « Anniversaires des contacts » est en lecture seule.

Dans votre application agenda, cliquer sur <i class="fa fa-bars" aria-hidden="true"></i> pour cocher les agendas à afficher. Les agendas sont alors présents dans votre application d'Agenda.

![Agenda](../images/davdroid-5.png)

<p class="alert-info alert"><b>Note</b> : les abonnements aux agendas externes (fichier `.ics`) effectués dans Framagenda sont détectés par DAVx⁵ mais il faut installer l'application ICSdroid (<a href="https://play.google.com/store/apps/details?id=at.bitfire.icsdroid">Google Play</a>, <a href="https://f-droid.org/repository/browse/?fdid=at.bitfire.icsdroid">F-Droid</a>).</p>

## Synchronisation des contacts existants du téléphone vers Framagenda

Les contacts déjà existants sur le téléphone ne sont pas synchronisés sur Framagenda car ils ne sont pas dans le même compte sur votre appareil Android. Il faut commencer par exporter vos contacts existants dans l'application Contacts de votre appareil, puis les réimporter dans le compte CalDAV.

Allez dans l'application Contacts, puis sélectionnez **Importer/Exporter** dans le menu. Choisissez **Exporter dans fichier VCF**, puis l'application vous demande où vous voulez enregistrer le fichier. Sélectionnez par exemple **Téléchargements**.

![Exportation des contacts](../images/davdroid-3.png)

Vous devez maintenant vous rendre dans vos téléchargements et ouvrir le fichier VCF. Si vous avez un choix de plusieurs applications pour ouvrir le fichier, choisissez l'application **Contacts** de base d'Android ou de Google. Il vous est demandé dans quel compte vous voulez importer ces contacts. Choisissez le compte DAVx⁵ que vous venez de créer.

![Importation des contacts](../images/davdroid-4.png)

Les contacts sont alors importés dans ce compte et progressivement téléversés sur Framagenda. Si vous avez des contacts en double, vous pouvez sélectionner les comptes à afficher dans le menu de l'application Contacts : **Contacts à afficher**.

## Synchronisation de la liste de tâches

<p class="alert-info alert">L'application s'appelle <b>OpenTasks</b> sur Google Play mais s'appelle simplement <b>Tasks</b> sur F-droid</p>

Pour synchroniser la liste des tâches de Framagenda sur votre appareil Android, vous devez installer l'application Tasks disponible gratuitement sur [F-Droid](https://f-droid.org/fr/packages/org.dmfs.tasks/) et [Google Play](https://play.google.com/store/apps/details?id=org.dmfs.tasks).

Une fois l'application OpenTasks installée, selon la version d'Android que vous possédez, DAVx⁵ va détecter automatiquement la nouvelle installation et demander l'autorisation de lire les tâches afin de les synchroniser avec Framagenda. Si ce n'est pas le cas, vous devez réinstaller (et reconfigurer) DAVx⁵ après avoir installé OpenTasks.

Vous pouvez alors ouvrir l'application OpenTasks. Les agendas que vous gérez sont affichés et vous pouvez créer des tâches associées. Si vous créez d'autres listes de tâches, il faut les activer individuellement dans l'application DAVx⁵ (comme pour les agendas).

![OpenTasks interface](../images/tasks-android-1.png)

Pour créer une tâche, sélectionnez le bouton **+** en bas à droite de l'écran, puis sélectionnez l'endroit où vous voulez créer une tâche (le compte local de l'appareil, un de vos agendas ou bien une de vos listes de tâches).

![Apercu d'une tâche](../images/tasks-android-2.png)
