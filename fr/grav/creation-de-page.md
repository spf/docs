# Créer et gérer les pages

## Créer une page

Pour créer une page :
* Cliquez sur **Pages** dans le menu Administrateur (à gauche). Apparaît alors l'arborescence des pages du site.
* Ensuite, cliquez sur **Ajouter** en haut à droite.
![Ajouter une page](images/Ajouterunepage.png)
* Remplissez les différents champs (ils seront modifiables ultérieurement si besoin)
	* **Titre de la page**
	* **Nom de dossier** : généré automatiquement à partir du titre, il servira notamment à l'URL de la page
	* **Page parente** : emplacement dans l'arborescence des pages
	* **Template** : à choisir dans un menu déroulant, pour du rédactionnel, prenons _Auteur_ ou _Blog_
	* **Visibilité** : pour le moment, ne rendons pas visible cette page, puisque nous allons la modifier.
* Vous arrivez sur la page ainsi créée. **Conseil : enregistrez tout de suite** (en haut à droite), cela permet d'avoir la possibilité d'ajouter des Médias (des images notamment).   
![Page nouvellement créée](images/Pagetouteneuve.png)

## Le composeur de pages
Le composeur de page comporte trois onglets : **contenu**, **options** et **avancé**.

Regardons de plus près l'onglet **contenu** qui permet d'écrire le texte, le mettre en forme et inclure médias et liens dans la page.
Écrivez le texte. Vous pouvez le mettre en forme avec les différents boutons (gras, italique, liste, lien...). Si vous passez la souris au-dessus de chaque bouton, une infobulle vous indique son usage.
Le texte mis en forme se met automatiquement en [syntaxe Markdown](https://docs.framasoft.org/fr/grav/markdown.html), c'est donc normal d'avoir des caractères supplémentaires qui apparaissent.
A droite, le bouton en forme d’œil permet d'avoir le résultat final, tandis que l’icône </> permet de revenir à l'éditeur de texte.
![Mon texte mis en forme en Markdown](images/Pages_contenu.png)
![La version visible sur la page](images/Page_contenu_visuel.png)

L'onglet **options** permet de définir les métadonnées de la page et les caractéristiques de publication : dates, mots-clé, catégorie... C'est donc dans ce menu qu'il sera possible de publier la page, une fois qu'elle sera terminée !

Enfin, l'onglet **avancé**, comme son nom l'indique, permet d'aller plus loin et de changer le nom de dossier (donc l'URL - attention !), la place de la page dans l'arborescence ou le template.

## Les images
Pour ajouter une image, il y a plusieurs possibilités :
* Si vous avez le lien vers l'image, placez le curseur où vous souhaitez mettre l'image, cliquez sur l’icône _image_ dans l'onglet contenu et indiquez-en le lien.
* Si votre image est stockée sur votre ordinateur, faites-en un glisser-déposer dans l'encart _Médias de la page_ qui se trouve sous l'éditeur de contenu. Il vous suffira de la déplacer dans l'éditeur de contenu là où vous souhaitez la positionner.

Deux astuces :
* La syntaxe des images est la suivante : `![descriptif de l'image](lien direct de l'image)`
* La première image de la rubrique **Médias de la page** illustrera automatiquement la page. Vous pouvez changer l'ordre des images en les cliquant-déposant.
